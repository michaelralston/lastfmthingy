package singletons

import java.io.PrintStream
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

import threads.Library
import threads.Neighbour
import entities.User

object Main {

    private const val SPREADSHEET_FILENAME = "/home/michael/albums.tsv"

    private var ps: PrintStream = PrintStream(Main.SPREADSHEET_FILENAME, "UTF-8")

    var tpe: ThreadPoolExecutor = ThreadPoolExecutor(
        10, 10, 3600, TimeUnit.SECONDS,
        LinkedBlockingQueue()
    )

    var rootUser: User = DataAccess.user("StratPlayedBlue")

    @JvmStatic
    fun main(args: Array<String>) {
        try {
            val library = Library(Main.rootUser)
            val neighbour = Neighbour(Main.rootUser)
            Main.tpe.execute(library)
            Main.tpe.execute(neighbour)

            Main.waitForThreads()

            DataAccess.printArtistList(Main.ps)
            Main.ps.flush()
            Main.ps.close()

            Main.tpe.shutdown()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Throws(InterruptedException::class)
    private fun waitForThreads() {
        Thread.sleep(10)
        while (!(Main.tpe.queue.isEmpty() && Main.tpe.activeCount == 0)) {
            println(
                "Queued:\t" + Main.tpe.queue.size
                        + "\tRunning:\t" + Main.tpe.activeCount + "\tCompleted:\t"
                        + Main.tpe.completedTaskCount + "\tTotal:\t"
                        + Main.tpe.taskCount
            )
            Thread
                .sleep(((Main.tpe.queue.size + Main.tpe.activeCount + 1) * 200).toLong())
        }
    }

}
