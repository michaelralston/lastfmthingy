package singletons

import java.io.PrintStream
import java.util.*

import entities.Artist
import entities.ArtistComparator
import entities.PlayCount
import entities.User
import kotlin.collections.ArrayList

object DataAccess {

    private val artistMap = HashMap<String, Artist>()
    private val user2playCountMap = HashMap<User, TreeMap<Artist, PlayCount>>()
    private val userMap = HashMap<String, User>()

    private var artistList: MutableList<Artist> = ArrayList()

    @Synchronized
    @Throws(Exception::class)
    fun artist(name: String): Artist {
        if (!DataAccess.artistMap.containsKey(name)) {
            DataAccess.artistMap[name] = Artist(name)
        }
        return DataAccess.artistMap[name]!!
    }

    @Synchronized
    @Throws(Exception::class)
    fun getPlayCount(
        user: User,
        artist: Artist
    ): PlayCount {
        val artist2PlayCountMap: TreeMap<Artist, PlayCount>
        if (!DataAccess.user2playCountMap.containsKey(user)) {
            DataAccess.user2playCountMap[user] = TreeMap()
        }
        artist2PlayCountMap = DataAccess.user2playCountMap[user]!!
        if (!artist2PlayCountMap.containsKey(artist)) {
            artist2PlayCountMap[artist] = PlayCount(user, artist)
        }
        return artist2PlayCountMap[artist]!!
    }

    @Synchronized
    @Throws(Exception::class)
    fun user(username: String): User {
        if (!DataAccess.userMap.containsKey(username)) {
            DataAccess.userMap[username] = User(username)
        }
        return DataAccess.userMap[username]!!
    }

    @Synchronized
    fun printArtistList(ps: PrintStream) {
        DataAccess.artistList = ArrayList()
        DataAccess.artistList.addAll(DataAccess.artistMap.values)

        Collections.sort(DataAccess.artistList, ArtistComparator())
        for (t in DataAccess.artistList) {
            val myPlay = Main.rootUser.playCounts[t]
            var count = 0.0
            if (myPlay != null) {
                count = myPlay.plays
            }
            ps.println(t.name + "\t" + count + "\t" + t.score)
        }
    }
}
