package singletons

import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.URL

import javax.xml.parsers.DocumentBuilderFactory

import org.w3c.dom.Document
import org.xml.sax.InputSource

object UrlToDoc {
    private var factory: DocumentBuilderFactory? = null
    private const val FETCH_INTERVAL = 1000
    private const val MAX_ATTEMPTS = 10
    private var nextFetch: Long = 0

    fun doc(urlString: String): Document? {
        var doc: Document? = null
        var iStream: InputStream? = null
        val isr: InputStreamReader? = null
        var bis: BufferedInputStream? = null
        var attempts = 0
        while (doc == null) {
            try {
                Thread.sleep((attempts * 100 + 1).toLong())
                attempts++
                if (attempts > UrlToDoc.MAX_ATTEMPTS) {
                    return null
                }

                val db = UrlToDoc.factory()!!.newDocumentBuilder()
                val url = URL(urlString)

                UrlToDoc.waitToFetch()
                val startTime = System.currentTimeMillis()
                val connection = url.openConnection()
                connection.readTimeout = 60000
                iStream = connection.getInputStream()
                val time = System.currentTimeMillis() - startTime
                println(time.toString() + ":\t" + urlString)
                Thread.yield()

                bis = BufferedInputStream(iStream)
                val iSource = InputSource(bis)
                iSource.encoding = "UTF-8"
                doc = db.parse(iSource)
            } catch (e: Exception) {
                println("UrlToDoc(String urlString = $urlString )")
                e.printStackTrace()
            } finally {
                if (bis != null) {
                    try {
                        bis.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
                if (isr != null) {
                    try {
                        isr.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
                if (iStream != null) {
                    try {
                        iStream.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
            }
        }
        return doc
    }

    @Synchronized
    private fun factory(): DocumentBuilderFactory? {
        if (UrlToDoc.factory == null) {
            UrlToDoc.factory = DocumentBuilderFactory.newInstance()
        }
        return UrlToDoc.factory
    }

    @Synchronized
    @Throws(InterruptedException::class)
    private fun waitToFetch() {
        val curTime = System.currentTimeMillis()
        if (curTime < UrlToDoc.nextFetch) {
            Thread.sleep(UrlToDoc.nextFetch - curTime)
        }
        UrlToDoc.nextFetch = System.currentTimeMillis() + UrlToDoc.FETCH_INTERVAL
        return
    }

}
