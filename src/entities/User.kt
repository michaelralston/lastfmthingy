package entities

import java.io.Serializable
import java.util.HashMap
import java.util.HashSet

/**
 * Entity implementation class for Entity: User
 *
 */
class User(var username: String) : Comparable<User>, Serializable {

    private var neighbours: MutableSet<User> = HashSet()

    var playCounts: MutableMap<Artist, PlayCount> = HashMap()

    var totalPlays: Double = 0.0

    @Synchronized
    fun addNeighbour(neighbour: User): Boolean {
        return this.neighbours.add(neighbour)
    }

    @Synchronized
    fun addPlayCount(playCount: PlayCount) {
        this.totalPlays += playCount.plays
        this.playCounts[playCount.artist] = playCount
    }

    override fun compareTo(other: User): Int {
        return this.username.compareTo(other.username)
    }

    override fun equals(other: Any?): Boolean {
        return this.username == (other as User).username
    }

    override fun hashCode(): Int {
        return username.hashCode()
    }

    companion object {
        private const val serialVersionUID = 1L
    }

}
