package entities

import singletons.Main
import java.io.Serializable

import java.util.HashSet

/**
 * Entity implementation class for Entity: Artist
 *
 */
class Artist(val name: String) : Serializable, Comparable<Artist> {

    private var plays: MutableSet<PlayCount> = HashSet()

    var score: Double = 0.0
        @Synchronized get() {
            field = 0.0
            for (play in this.plays) {
                if (play.user != Main.rootUser) {
                    field += play.score
                }
            }
            return field
        }

    @Synchronized
    fun addPlayCount(play: PlayCount): Boolean {
        return this.plays.add(play)
    }

    override fun compareTo(other: Artist): Int {
        return this.name.compareTo(other.name)
    }

    override fun equals(other: Any?): Boolean {
        return if (other is Artist) {
            this.name == other.name
        } else {
            false
        }
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    companion object {
        private const val serialVersionUID = 1L
    }

}
