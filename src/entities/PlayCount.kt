package entities

import java.io.Serializable

/**
 * Entity implementation class for Entity: PlayCount
 *
 */
class PlayCount(var user: User, var artist: Artist) : Serializable {

    var plays = 0.0

    val score: Double
        get() = this.plays / this.user.totalPlays

    companion object {
        private const val serialVersionUID = 1L
    }

}
