package entities

import java.util.Comparator

class ArtistComparator : Comparator<Artist> {

    override fun compare(o1: Artist, o2: Artist): Int {
        return o1.score.compareTo(o2.score)
    }

}
