package threads

import java.net.URLEncoder

import org.w3c.dom.Element

import singletons.DataAccess
import singletons.Main
import singletons.UrlToDoc
import entities.User
import org.jsoup.Jsoup
import org.w3c.dom.NodeList
import java.util.regex.Pattern
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory

class Neighbour(private val user: User) : Thread() {

    override fun run() {
        try {
            val doc = Jsoup.connect(
                    "https://www.last.fm/user/"
                            + URLEncoder.encode(this.user.username, "UTF-8")
                            + "/neighbours"
                ).get()
            if (doc != null) {
                val elements = doc.select(".user-list-link")
                for (e in elements) {
                    val url = e.attr("href")
                    val matcher = Pattern.compile("/user/(.+?)\$").matcher(url)
                    if (matcher.find()) {
                        try {
                            val neighbour = DataAccess.user(matcher.group(1))
                            this.user.addNeighbour(neighbour)
                            neighbour.addNeighbour(this.user)

                            val library = Library(neighbour)
                            Main.tpe.execute(library)
                            Thread.yield()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
