package threads

import java.net.URLEncoder

import org.w3c.dom.*

import singletons.DataAccess
import singletons.Main
import singletons.UrlToDoc
import entities.User

class Library : Thread {
    private val user: User

    private val page: Int

    constructor(user: User) {
        this.user = user
        this.page = 0
    }

    constructor(user: User, page: Int) {
        this.user = user
        this.page = page
    }

    override fun run() {
        try {
            val doc: Document?
            if (this.page == 0) {
                doc = UrlToDoc
                    .doc(
                        "http://ws.audioscrobbler.com/2.0/?method=library.getartists&user="
                                + URLEncoder.encode(this.user.username, "UTF-8")
                                + "&api_key=23c225363355011adde1156cc5fd74c9"
                    )
                if (doc != null) {
                    val artistNodeList = doc.getElementsByTagName("artists")
                    val artistNode = artistNodeList.item(0)
                    val attributes = artistNode.attributes
                    val pagesNode = attributes.getNamedItem("totalPages")
                    val pagesString = pagesNode.nodeValue
                    val numPages = Integer.valueOf(pagesString)!!
                    var i = 2
                    while (i < numPages && i < 11) {
                        val library = Library(this.user, i)
                        Main.tpe.execute(library)
                        Thread.yield()
                        i++
                    }
                }
            } else {
                doc = UrlToDoc
                    .doc(
                        "http://ws.audioscrobbler.com/2.0/?method=library.getartists&user="
                                + URLEncoder.encode(this.user.username, "UTF-8")
                                + "&page="
                                + this.page
                                + "&api_key=23c225363355011adde1156cc5fd74c9"
                    )
            }
            if (doc != null) {
                val artistNodeList = doc.getElementsByTagName("artist")
                for (i in 0 until artistNodeList.length) {
                    val artistElement = artistNodeList.item(i) as Element
                    val artistName = artistElement.getElementsByTagName("name")
                        .item(0).firstChild.nodeValue
                    val playCount = java.lang.Double.valueOf(
                        artistElement
                            .getElementsByTagName("playcount").item(0).firstChild
                            .nodeValue
                    )
                    if (playCount > 0) {
                        try {
                            val album = DataAccess.artist(artistName)
                            val pc = DataAccess.getPlayCount(this.user, album)
                            album.addPlayCount(pc)
                            pc.plays = playCount
                            this.user.addPlayCount(pc)

                            Thread.yield()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
